package org.bitbucket.xumr;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.InvocationHandler;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;

import static java.util.Collections.unmodifiableMap;

/**
 * @author umur
 */
@SuppressWarnings("WeakerAccess")
public class GetterHandler<T> implements InvocationHandler {

    private boolean mapping = false;
    private Map<String, Object> fieldMap;
    private T object;
    private T proxy;

    private GetterHandler(T object) {
        this.object = object;
    }

    @SuppressWarnings("unchecked")
    public static <T> GetterHandler<T> create(T object) {
        GetterHandler<T> handler = new GetterHandler<>(object);
        handler.proxy = (T) Enhancer.create(object.getClass(), handler);

        return handler;
    }

    @SafeVarargs
    public final Map<String, Object> select(Function<T, Object>... getters) {
        fieldMap = new LinkedHashMap<>(getters.length);

        for (Function<T, Object> getter : getters) {
            mapping = true;
            getter.apply(proxy);
            mapping = false;
        }

        return unmodifiableMap(fieldMap);
    }

    private String methodToKey(Method method) {
        String name = method.getName();

        if (name.startsWith("get")) {
            name = name.substring(3);
        }
        else if (name.startsWith("is")) {
            name = name.substring(2);
        }
        else {
            return null;
        }
        return name.substring(0, 1).toLowerCase(Locale.ENGLISH) + name.substring(1);
    }

    public Object invoke(Object o, Method method, Object[] args) throws Throwable {
        Object value = method.invoke(object);

        if (mapping) {
            String key = methodToKey(method);
            if (key != null) {
                fieldMap.put(key, value);
            }
        }
        return value;
    }
}
