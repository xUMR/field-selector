package org.bitbucket.xumr;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author umur
 */
public class GetterHandlerTest {

    private Person person;

    // region Warm up
    @BeforeClass
    public static void setUpOnce() throws Exception {
        Person person = new Person(1234567890, "John", "Doe", "john@doe.com");
        GetterHandler.create(person).select(Person::getId);
    }
    // endregion

    @Before
    public void setUp() throws Exception {
        person = new Person(1234567890, "John", "Doe", "john@doe.com");
    }

    @Test
    public void test1() throws Exception {
        Map<String, Object> nameSurname = GetterHandler.create(person)
                .select(Person::getName, Person::getSurname);

        assertEquals("John", nameSurname.get("name"));
        assertEquals("Doe", nameSurname.get("surname"));
        assertNull(nameSurname.get("id"));
        assertNull(nameSurname.get("email"));
    }

    @Test
    public void test2() throws Exception {
        Map<String, Object> idSurname = GetterHandler.create(person)
                .select(Person::getId, Person::getSurname);

        assertEquals(1234567890L, idSurname.get("id"));
        assertEquals("Doe", idSurname.get("surname"));
        assertNull(idSurname.get("name"));
        assertNull(idSurname.get("email"));
    }

    @Test
    public void test3() throws Exception {
        Map<String, Object> idEmail = GetterHandler.create(person)
                .select(Person::getId, Person::getEmail);

        assertEquals(1234567890L, idEmail.get("id"));
        assertEquals("john@doe.com", idEmail.get("email"));
        assertNull(idEmail.get("name"));
        assertNull(idEmail.get("surname"));

        Map<String, Object> nameEmail = GetterHandler.create(person)
                .select(Person::getName, Person::getEmail);

        assertEquals("John", nameEmail.get("name"));
        assertEquals("john@doe.com", idEmail.get("email"));
        assertNull(nameEmail.get("id"));
        assertNull(nameEmail.get("surname"));
    }

    private static class Person {
        private long id;
        private String name;
        private String surname;
        private String email;

        // required
        Person() {}

        Person(long id, String name, String surname, String email) {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.email = email;
        }

        // region Getters & Setters
        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
        // endregion
    }
}
