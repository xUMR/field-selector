Select fields of objects and put them into Map<String, Object> using getters:

```
Person person = new Person(1234567890, "John", "Doe", "john@doe.com");

Map<String, Object> nameSurname = GetterHandler.create(person)
        .select(Person::getName, Person::getSurname);

assertEquals("John", nameSurname.get("name"));
assertEquals("Doe", nameSurname.get("surname"));
assertNull(nameSurname.get("id"));
assertNull(nameSurname.get("email"));
```

To add this project as a dependency, add the following to pom.xml file:
```
<repositories>
    <repository>
        <id>org.bitbucket.xumr.extensions</id>
        <url>https://dl.dropboxusercontent.com/u/126445027/mvn</url>
    </repository>
</repositories>

<dependencies>
    ...
    <dependency>
        <groupId>org.bitbucket.xumr</groupId>
        <artifactId>field-selector</artifactId>
        <version>0.2.0</version>
    </dependency>
    ...
</dependencies>
```

## License
[MIT LICENSE](LICENSE.md)
